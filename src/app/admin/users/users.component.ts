import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styles: []
})
export class UsersComponent implements OnInit {

  users = [];

  constructor(private usersService: UsersService) { }

  ngOnInit() {
    this.getUsers();
  }

  async getUsers() {
    let token = sessionStorage.getItem('token');
    try {
      var rs: any = await this.usersService.getUsers(token);
      if (rs.ok) {
        this.users = rs.rows;
      } else {
        console.log(rs.error);
      }
    } catch (error) {
      console.log(error);
    }
  }

}
